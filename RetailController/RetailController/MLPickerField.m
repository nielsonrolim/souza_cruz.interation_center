//
//  MLPickerField.m
//  RetailController
//
//  Created by Nielson Rolim on 9/4/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLPickerField.h"

@interface MLPickerField ()

//Categories Picker View Keyboard
@property (strong, nonatomic) UIPickerView* pickerView;

@end

@implementation MLPickerField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.delegate = self;
        self.inputView = self.pickerView;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.delegate = self;
        self.inputView = self.pickerView;
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.delegate = self;
        self.inputView = self.pickerView;
    }
    return self;
}

- (NSArray*) options {
    if (!_options) {
        _options = @[@"Option #1", @"Option #2", @"Option #3", @"Option #4", @"Option #5"];
    }
    return _options;
}

- (UIPickerView*) pickerView {
    if (!_pickerView) {
        _pickerView = [[UIPickerView alloc] init];
        _pickerView.dataSource = self;
        _pickerView.delegate = self;
    }
    return _pickerView;
}


- (void) textFieldDidBeginEditing:(UITextField *)textField {
    if ([self.text isEqualToString:@""]) {
        self.text = self.options[self.defaultOptionIndex];
        [self.pickerView selectRow:self.defaultOptionIndex inComponent:0 animated:NO];
    }
}

// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.options.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.options[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component {
    self.text = self.options[row];
}



@end
