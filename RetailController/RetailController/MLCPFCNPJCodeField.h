//
//  MLCPFCNPJCodeField.h
//  RetailController
//
//  Created by Nielson Rolim on 9/1/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLNumberPadTextField.h"

/*
 A custom TextField to input done button in NumberPad and use mask for CPF, CNPJ or Code depending on number of chars
 */

@interface MLCPFCNPJCodeField : MLNumberPadTextField <UITextFieldDelegate>

@end
