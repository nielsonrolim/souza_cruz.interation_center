//
//  MLPickerField.h
//  RetailController
//
//  Created by Nielson Rolim on 9/4/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLTextField.h"

@interface MLPickerField : MLTextField <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

//Categories for the Picker View (Should be replaced in the class that's using MLPickerField)
@property (strong, nonatomic) NSArray* options;

//Default option index
@property (nonatomic) NSInteger defaultOptionIndex;

@end
