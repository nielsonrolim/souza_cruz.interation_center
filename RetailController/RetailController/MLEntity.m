//
//  MLEntitie.m
//  RetailController
//
//  Created by Delano Oliveira on 13/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLEntity.h"

@implementation MLEntity

+ (id)entityFromDictionary:(NSDictionary *)dictionary
{
    MLEntity *entity = [[MLEntity alloc] initWithDictionary:dictionary];
    return entity;
}

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self){
        [self setValuesForKeysWithDictionary:dictionary];
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    if([key isEqualToString:@"id"]){
        key = @"id_entity";
    }
    [super setValue:value forKey:key];
}

@end
