//
//  MLAdvertisingViewController.m
//  RetailController
//
//  Created by Nielson Rolim on 9/4/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLAdvertisingViewController.h"
#import "MBProgressHUD.h"
#import "MLHTTPRequestOperationManager.h"
#import "MLUtil.h"

@interface MLAdvertisingViewController ()

//Ad Button
@property (weak, nonatomic) IBOutlet UIButton *advertisingButton;

@property (strong, nonatomic) NSString* adLink;

//Method trigged when Ad Button is pressed - Opens the Ad's link
- (IBAction)adButtonPressed:(UIButton *)sender;

@end

@implementation MLAdvertisingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self getAd];
}


- (void) getAd {
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    MLHTTPRequestOperationManager *manager = [MLHTTPRequestOperationManager manager];
    
    MLHTTPRequestParameters *parameters = [MLHTTPRequestParameters new];
    
    [manager GET:kSERVER_GET_AD_PATH
      parameters:parameters.dictionary
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSDictionary *json = responseObject;
             if(json[kSERVER_JSON_DATA_PATH]){
                 NSString* thumbAdURL = json[kSERVER_JSON_DATA_PATH][@"full"];
                 self.adLink = json[kSERVER_JSON_DATA_PATH][@"link"];
                 
                 UIImage* thumbAd = [MLUtil imageFromURL:thumbAdURL];
                 
                 [self.advertisingButton setImage:thumbAd forState:UIControlStateNormal];
                 
                 [progressHUD hideUsingAnimation:YES];
             }
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [progressHUD hideUsingAnimation:YES];
         }];
}

- (IBAction)adButtonPressed:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.adLink]];
}

@end
