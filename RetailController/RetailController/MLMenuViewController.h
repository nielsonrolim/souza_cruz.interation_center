//
//  MLMenuViewController.h
//  RetailController
//
//  Created by Delano Oliveira on 14/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MLMenuViewController;

/*
 Protocol to controller menu buttons.
*/

@protocol MLMenuViewControllerDelegate <NSObject>

@optional

// Call methods when the respective button was pressed.
- (void)menuViewControllerDidLogoutButtonPressed:(MLMenuViewController*)controller;
- (void)menuViewControllerDidHomeButtonPressed:(MLMenuViewController*)controller;
- (void)menuViewControllerDidAccountButtonPressed:(MLMenuViewController*)controller;
- (void)menuViewControllerDidAppRateButtonPressed:(MLMenuViewController*)controller;
- (void)menuViewControllerDidAboutButtonPressed:(MLMenuViewController*)controller;

@end

/*
 Controller to Menu View
*/

@interface MLMenuViewController : UIViewController

// Represent the delegate to protocol
@property (weak) id<MLMenuViewControllerDelegate> delegate;

//Method executed to log out the restricted area
- (IBAction)logoutButtonPressed:(id)sender;

// Method executed when Home button is pressed. This method call the respective delegate method.
- (IBAction)homeButtonPressed:(id)sender;
// Method executed when Account button is pressed. This method call the respective delegate method.
- (IBAction)accountButtonPressed:(id)sender;
// Method executed when App Rate button is pressed. This method call the respective delegate method.
- (IBAction)appRateButtonPressed:(id)sender;
// Method executed when About button is pressed. This method call the respective delegate method.
- (IBAction)aboutButtonPressed:(id)sender;

@end
