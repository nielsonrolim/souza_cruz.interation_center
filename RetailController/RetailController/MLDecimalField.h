//
//  MLDecimalField.h
//  RetailController
//
//  Created by Nielson Rolim on 8/27/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLNumberPadTextField.h"

/*
A custom TextField to input done button in NumberPad and use mask for decimal numbers
*/

@interface MLDecimalField : MLNumberPadTextField <UITextFieldDelegate>

@end
