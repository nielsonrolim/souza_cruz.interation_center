//
//  MLRequestProductViewController.m
//  RetailController
//
//  Created by Nielson Rolim on 8/14/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLRequestProductCallViewController.h"
#import "MLAppDelegate.h"
#import "MLRetailer.h"
#import "MBProgressHUD.h"
#import "MLHTTPRequestOperationManager.h"
#import "MLUtil.h"

@interface MLRequestProductCallViewController ()

//Shared Retailer Object
@property (weak, nonatomic) MLRetailer* retailer;

@end

@implementation MLRequestProductCallViewController

- (MLRetailer*) retailer {
    if (!_retailer) {
        MLAppDelegate* appDelegate = (MLAppDelegate*)[[UIApplication sharedApplication] delegate];
        _retailer = appDelegate.retailer;
    }
    return _retailer;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)sendButtonPressed:(UIButton *)sender {
    UIAlertView* confirm = [MLUtil confirmDialog];
    confirm.delegate = self;
    [confirm show];
}

- (void) sendRequest {
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    MLHTTPRequestOperationManager *manager = [MLHTTPRequestOperationManager manager];
    
    MLHTTPRequestParameters *parameters = [MLHTTPRequestParameters new];
    [parameters addParameterKey:kSERVER_PARAMETER_TOKEN
                       andValue:self.retailer.token];
    
    [manager POST:kSERVER_PRODUCT_REQUEST_PATH
       parameters:parameters.dictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              [progressHUD hideUsingAnimation:YES];
              NSDictionary *json = responseObject;
              if(json[kSERVER_JSON_DATA_PATH]){
                  NSString* successMessage = NSLocalizedString(@"PRODUCT_REQUEST_SUCCESS", @"Product requested successfully");
                  [MLUtil showAlertMessage:successMessage];
                  [self.navigationController popToRootViewControllerAnimated:TRUE];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [progressHUD hideUsingAnimation:YES];
              NSString* errorMessage = NSLocalizedString(@"PRODUCT_REQUEST_ERROR", @"Product request error");
              [MLUtil showAlertMessage:errorMessage];
          }];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // Handle interaction
    switch (buttonIndex) {
        case 0: //Cancel
            break;
        case 1: //OK
            [self sendRequest];
            break;
    }
}

@end
