//
//  MLRequestPaymentSlipViewController.m
//  RetailController
//
//  Created by Nielson Rolim on 8/27/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLRequestPaymentSlipViewController.h"
#import "MLAppDelegate.h"
#import "MLRetailer.h"
#import "MBProgressHUD.h"
#import "MLHTTPRequestOperationManager.h"
#import "MLUtil.h"

@interface MLRequestPaymentSlipViewController ()

//Shared Retailer Object
@property (weak, nonatomic) MLRetailer* retailer;

//Billet View
@property (weak, nonatomic) IBOutlet UIView *billetView;

//Method to show Billet View
- (IBAction)showBilletView:(UIButton *)sender;

//Method to hide Billet View
- (IBAction)hideBilletView:(UIButton *)sender;

@end

@implementation MLRequestPaymentSlipViewController

- (MLRetailer*) retailer {
    if (!_retailer) {
        MLAppDelegate* appDelegate = (MLAppDelegate*)[[UIApplication sharedApplication] delegate];
        _retailer = appDelegate.retailer;
    }
    return _retailer;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [NSLocale currentLocale];

}

- (IBAction)sendButtonPressed:(UIButton *)sender {
    [self.view endEditing:YES];
    
    if (![self validateFields]) {
        return;
    }
    
    UIAlertView* confirm = [MLUtil confirmDialog];
    confirm.delegate = self;
    [confirm show];
    
}

- (BOOL) validateFields {
    NSString* errorTitle = NSLocalizedString(@"VALIDATION_ERROR_TITLE", @"Error Title");
    
    if ([self.emailTextField.text isEqualToString:@""]) {
        NSString* errorMessage = NSLocalizedString(@"EMAIL_BLANK", @"E-mail empty");
        [MLUtil showAlertMessage:errorMessage withTitle:errorTitle];
        return NO;
    }
    
    if (![MLUtil validateEmail:self.emailTextField.text]) {
        NSString* errorMessage = NSLocalizedString(@"EMAIL_INVALID", @"E-mail address is invalid");
        [MLUtil showAlertMessage:errorMessage withTitle:errorTitle];
        return NO;
    }
    
    if ([self.invoiceTextField.text isEqualToString:@""]) {
        NSString* errorMessage = NSLocalizedString(@"INVOICE_BLANK_SINGLE_FIELD", @"Invoice empty");
        [MLUtil showAlertMessage:errorMessage withTitle:errorTitle];
        return NO;
    }
    
    if ([self.valueTextField.text isEqualToString:@""]) {
        NSString* errorMessage = NSLocalizedString(@"VALUE_BLANK", @"Invoice empty");
        [MLUtil showAlertMessage:errorMessage withTitle:errorTitle];
        return NO;
    }
    
    if ([self.dueDateTextField.text isEqualToString:@""]) {
        NSString* errorMessage = NSLocalizedString(@"DUE_DATE_BLANK", @"Invoice empty");
        [MLUtil showAlertMessage:errorMessage withTitle:errorTitle];
        return NO;
    }
    return YES;
}

- (IBAction)showBilletView:(UIButton *)sender {
    self.billetView.hidden = NO;
}

- (IBAction)hideBilletView:(UIButton *)sender {
    self.billetView.hidden = YES;
}

- (void) sendRequest {
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    MLHTTPRequestOperationManager *manager = [MLHTTPRequestOperationManager manager];
    
    MLHTTPRequestParameters *parameters = [MLHTTPRequestParameters new];
    [parameters addParameterKey:kSERVER_PARAMETER_TOKEN
                       andValue:self.retailer.token];
    [parameters addParameterKey:kSERVER_REQUEST_PAYMENT_SLIP_PARAMETER_EMAIL
                       andValue:self.emailTextField.text];
    [parameters addParameterKey:kSERVER_REQUEST_PAYMENT_SLIP_PARAMETER_INVOICE
                       andValue:self.invoiceTextField.text];
    [parameters addParameterKey:kSERVER_REQUEST_PAYMENT_SLIP_PARAMETER_VALUE
                       andValue:self.valueTextField.text];
    [parameters addParameterKey:kSERVER_REQUEST_PAYMENT_SLIP_PARAMETER_DUE_DATE
                       andValue:self.dueDateTextField.text];
    
    [manager POST:kSERVER_REQUEST_PAYMENT_SLIP_PATH
       parameters:parameters.dictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              [progressHUD hideUsingAnimation:YES];
              NSDictionary *json = responseObject;
              if(json[kSERVER_JSON_DATA_PATH]){
                  NSString* successMessage = NSLocalizedString(@"REQUEST_PAYMENT_SLIP_SUCCESS", @"Payment Slip requested successfully");
                  [MLUtil showAlertMessage:successMessage];
                  [self.navigationController popToRootViewControllerAnimated:TRUE];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"error: %@", error);
              [progressHUD hideUsingAnimation:YES];
              NSString* errorMessage = NSLocalizedString(@"REQUEST_PAYMENT_SLIP_ERROR", @"Payment Slip request error");
              [MLUtil showAlertMessage:errorMessage];
          }];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // Handle interaction
    switch (buttonIndex) {
        case 0: //Cancel
            break;
        case 1: //OK
            [self sendRequest];
            break;
    }
}

@end
