//
//  MLHomeViewController.h
//  RetailController
//
//  Created by Delano Oliveira on 29/07/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLMenuViewController.h"

/*
  Controller to Home View
*/

@interface MLHomeViewController : UIViewController <MLMenuViewControllerDelegate>

//Retailer Fancy Name Label
@property (weak, nonatomic) IBOutlet UILabel *fancyNameLabel;

//Retailer Company Name Label
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;

//Retailer Code (CNPJ) Label
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;


//Method to open and close the site menu
- (IBAction)openOrCloseMenu:(id)sender;

@end
