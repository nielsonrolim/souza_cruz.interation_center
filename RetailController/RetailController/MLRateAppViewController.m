//
//  MLRateAppViewController.m
//  RetailController
//
//  Created by Nielson Rolim on 8/19/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLRateAppViewController.h"
#import "MLAppDelegate.h"
#import "MLRetailer.h"
#import "MBProgressHUD.h"
#import "MLHTTPRequestOperationManager.h"
#import "MLUtil.h"

@interface MLRateAppViewController ()

//Categories for the Picker View
@property (strong, nonatomic) NSArray* categoriesArray;

//Placeholder for Comments Text View
@property (strong, nonatomic) NSString* commentsTextViewPlaceholder;

//Shared Retailer Object
@property (strong, nonatomic) MLRetailer* retailer;

@end

@implementation MLRateAppViewController

- (NSArray*) categoriesArray {
    if (!_categoriesArray) {
        _categoriesArray = @[@"Muito Fraco", @"Fraco", @"Regular", @"Bom", @"Muito bom"];
    }
    return _categoriesArray;
}

- (NSString*) commentsTextViewPlaceholder {
    if (!_commentsTextViewPlaceholder) {
        _commentsTextViewPlaceholder = NSLocalizedString(@"RATE_APP_COMMENTS_PLACEHOLDER", @"Comments placeholder");
    }
    return _commentsTextViewPlaceholder;
}

- (MLRetailer*) retailer {
    if (!_retailer) {
        MLAppDelegate* appDelegate = (MLAppDelegate*)[[UIApplication sharedApplication] delegate];
        _retailer = appDelegate.retailer;
    }
    return _retailer;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.categoryTextField.options = self.categoriesArray;
    self.categoryTextField.defaultOptionIndex = 2;

    self.commentsTextView.delegate = self;
    self.commentsTextView.text = self.commentsTextViewPlaceholder;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:self.commentsTextViewPlaceholder]) {
        textView.textColor = [UIColor blackColor];
        textView.text = @"";
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:self.commentsTextViewPlaceholder] || [textView.text isEqualToString:@""]) {
        textView.textColor = [UIColor lightGrayColor];
        textView.text = self.commentsTextViewPlaceholder;
    }
}

- (IBAction)sendButtonPressed:(id)sender {
    [self.view endEditing:YES];
    
    if (![self validateFields]) {
        return;
    }
    
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    MLHTTPRequestOperationManager *manager = [MLHTTPRequestOperationManager manager];
    
    MLHTTPRequestParameters *parameters = [MLHTTPRequestParameters new];
    [parameters addParameterKey:kSERVER_PARAMETER_TOKEN
                       andValue:self.retailer.token];
    [parameters addParameterKey:kSERVER_RATE_APP_PARAMETER_TYPE
                       andValue:self.categoryTextField.text];
    [parameters addParameterKey:kSERVER_RATE_APP_PARAMETER_DESCRIPTION
                       andValue:self.commentsTextView.text];
    [parameters addParameterKey:kSERVER_RATE_APP_PARAMETER_EMAIL
                       andValue:self.emailTextField.text];

    [manager POST:kSERVER_RATE_APP_PATH
       parameters:parameters.dictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              [progressHUD hideUsingAnimation:YES];
              NSDictionary *json = responseObject;
              if(json[kSERVER_JSON_DATA_PATH]){
                  NSString* success_massage = NSLocalizedString(@"RATE_APP_SUCCESS", @"App rated successfully");
                  [MLUtil showAlertMessage:success_massage];
                  
                  [self.navigationController popToRootViewControllerAnimated:TRUE];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [progressHUD hideUsingAnimation:YES];
              NSString* error_message = NSLocalizedString(@"RATE_APP_ERROR", @"Error while trying to rate app");
              [MLUtil showAlertMessage:error_message];
          }];
}

- (BOOL) validateFields {
    NSString* errorTitle = NSLocalizedString(@"VALIDATION_ERROR_TITLE", @"Error Title");
    
    if ([self.categoryTextField.text isEqualToString:@""]) {
        NSString* errorMessage = NSLocalizedString(@"RATE_APP_CATEGORY_BLANK", @"Category empty");
        [MLUtil showAlertMessage:errorMessage withTitle:errorTitle];
        return NO;
    }
    
    if (![MLUtil validateEmail:self.emailTextField.text]) {
        NSString* errorMessage = NSLocalizedString(@"EMAIL_INVALID", @"E-mail address is invalid");
        [MLUtil showAlertMessage:errorMessage withTitle:errorTitle];
        return NO;
    }
    
    return YES;
}

@end
