//
//  MLHomeViewController.m
//  RetailController
//
//  Created by Delano Oliveira on 29/07/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLHomeViewController.h"
#import "UIViewController+MMDrawerController.h"

#import "MLAboutViewController.h"
#import "MLAppDelegate.h"
#import "MLRetailer.h"
#import "MBProgressHUD.h"
#import "MLHTTPRequestOperationManager.h"
#import "MLUtil.h"

@interface MLHomeViewController ()

// Create a link (pointer) to DrawerController
@property (nonatomic, weak) MMDrawerController *drawerController;

//Retailer object from App Delegate
@property (strong, nonatomic) MLRetailer* retailer;

//Ad button
@property (weak, nonatomic) IBOutlet UIButton *advertisingButton;

//Summary of Retail Data
@property (weak, nonatomic) IBOutlet UIView *retailDataSummaryView;

//Retail Data Button
@property (weak, nonatomic) IBOutlet UIButton *retailDataButton;

//View that groups all actions
@property (weak, nonatomic) IBOutlet UIView *actionsView;

@end

@implementation MLHomeViewController

- (MLRetailer*) retailer {
    if (!_retailer) {
        MLAppDelegate* appDelegate = (MLAppDelegate*)[[UIApplication sharedApplication] delegate];
        _retailer = appDelegate.retailer;
    }
    return _retailer;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.drawerController = self.mm_drawerController;
    [self.drawerController setMaximumLeftDrawerWidth:kVALUE_MAX_MENU_WIDHT];
    
    MLMenuViewController *menuViewController = (MLMenuViewController*)self.drawerController.leftDrawerViewController;
    menuViewController.delegate = self;
    
    if (!IS_IPHONE5) {
        //iPhone  3.5 inch
        self.retailDataButton.hidden = YES;
        self.retailDataSummaryView.backgroundColor = [UIColor clearColor];
        self.retailDataSummaryView.alpha = 1.0f;
        [MLUtil reposition:self.retailDataSummaryView increasingXBy:0 andYBy:-12];
        [MLUtil reposition:self.actionsView increasingXBy:0 andYBy:-75];
    }
    
    self.fancyNameLabel.text = self.retailer.fancy_name;
    self.companyNameLabel.text = self.retailer.company_name;
    self.usernameLabel.text = [MLUtil formatCNPJorCPF:self.retailer.username];
    
    [self getAd];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];

    if ([self.retailer.change_password integerValue] == 1) {
        [self performSegueWithIdentifier:@"sendToRequestChangePassword" sender:self];
    }
    
    if ([self.retailer.terms_accepted integerValue] == 0) {
        [self performSegueWithIdentifier:@"sendToTermsOfUse" sender:self];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeNone];
}

- (void) getAd {
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    MLHTTPRequestOperationManager *manager = [MLHTTPRequestOperationManager manager];
    
    MLHTTPRequestParameters *parameters = [MLHTTPRequestParameters new];
    
    [manager GET:kSERVER_GET_AD_PATH
      parameters:parameters.dictionary
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSDictionary *json = responseObject;
             if(json[kSERVER_JSON_DATA_PATH]){
                 NSString* thumbAdURL = json[kSERVER_JSON_DATA_PATH][@"thumb"];
                 
                 UIImage* thumbAd = [MLUtil imageFromURL:thumbAdURL];
                 
                 [self.advertisingButton setImage:thumbAd forState:UIControlStateNormal];
                 
                 [progressHUD hideUsingAnimation:YES];
             }
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [progressHUD hideUsingAnimation:YES];
         }];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"sendToMyAccount"]){
        ;
    }
    else if([segue.identifier isEqualToString:@"sendToAbout"]){
        MLAboutViewController *aboutViewController = segue.destinationViewController;
        aboutViewController.hideCancelButton = YES;
    }
}

#pragma mark - Menu Delegate

- (void)menuViewControllerDidLogoutButtonPressed:(MLMenuViewController *)controller
{
    self.retailer = nil;
    [self openOrCloseMenu:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)menuViewControllerDidHomeButtonPressed:(MLMenuViewController *)controller
{
    [self openOrCloseMenu:nil];
}

- (void)menuViewControllerDidAccountButtonPressed:(MLMenuViewController *)controller
{
    [self openOrCloseMenu:nil];
    [self performSegueWithIdentifier:@"sendToAccount" sender:nil];
}

- (void)menuViewControllerDidAppRateButtonPressed:(MLMenuViewController *)controller
{
    [self openOrCloseMenu:nil];
    [self performSegueWithIdentifier:@"sendToAppRate" sender:nil];
}

- (void)menuViewControllerDidAboutButtonPressed:(MLMenuViewController *)controller
{
    [self openOrCloseMenu:nil];
    [self performSegueWithIdentifier:@"sendToAbout" sender:nil];
}

#pragma mark - IBActions

- (void)openOrCloseMenu:(id)sender
{
    if(self.drawerController.openSide != MMDrawerSideLeft){
        [self.drawerController openDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    }else{
        [self.drawerController closeDrawerAnimated:YES completion:nil];
    }
}


@end
