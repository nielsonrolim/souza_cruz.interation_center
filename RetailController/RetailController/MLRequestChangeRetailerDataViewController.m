//
//  MLRequestChangeRetailDataViewController.m
//  RetailController
//
//  Created by Nielson Rolim on 8/25/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLRequestChangeRetailerDataViewController.h"
#import "MLAppDelegate.h"
#import "MLRetailer.h"
#import "MBProgressHUD.h"
#import "MLHTTPRequestOperationManager.h"
#import "MLUtil.h"

@interface MLRequestChangeRetailerDataViewController ()

//Default placeholder for changeDataTextView
@property (strong, nonatomic) NSString* changeDataTextViewPlaceholder;

//Shared Retailer Object
@property (weak, nonatomic) MLRetailer* retailer;

@end

@implementation MLRequestChangeRetailerDataViewController

- (MLRetailer*) retailer {
    if (!_retailer) {
        MLAppDelegate* appDelegate = (MLAppDelegate*)[[UIApplication sharedApplication] delegate];
        _retailer = appDelegate.retailer;
    }
    return _retailer;
}

- (NSString*) changeDataTextViewPlaceholder {
    if (!_changeDataTextViewPlaceholder) {
        _changeDataTextViewPlaceholder = NSLocalizedString(@"CHANGE_DATA_TEXTVIEW_PLACEHOLDER", "changeDataTextView Placeholder");
    }
    return _changeDataTextViewPlaceholder;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.changeDataTextView.delegate = self;
    self.changeDataTextView.text = self.changeDataTextViewPlaceholder;
}


- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:self.changeDataTextViewPlaceholder]) {
        textView.textColor = [UIColor darkGrayColor];
        textView.text = @"";
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:self.changeDataTextViewPlaceholder] || [textView.text isEqualToString:@""]) {
        textView.textColor = [UIColor lightGrayColor];
        textView.text = self.changeDataTextViewPlaceholder;
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)sendButtonPressed:(id)sender {
    [self.view endEditing:YES];
    
    UIAlertView* confirm = [MLUtil confirmDialog];
    confirm.delegate = self;
    [confirm show];
}

- (void) sendRequest {
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    MLHTTPRequestOperationManager *manager = [MLHTTPRequestOperationManager manager];
    
    MLHTTPRequestParameters *parameters = [MLHTTPRequestParameters new];
    [parameters addParameterKey:kSERVER_PARAMETER_TOKEN
                       andValue:self.retailer.token];
    [parameters addParameterKey:kSERVER_PARAMETER_DESCRIPTION
                       andValue:self.changeDataTextView.text];
    
    [manager POST:kSERVER_RETAILER_DATA_UPDATE_PATH
       parameters:parameters.dictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              [progressHUD hideUsingAnimation:YES];
              NSDictionary *json = responseObject;
              if(json[kSERVER_JSON_DATA_PATH]){
                  NSString* successMessage = NSLocalizedString(@"RETAILER_DATA_UPDATE_SUCCESS", @"Password changed successfully");
                  [MLUtil showAlertMessage:successMessage];
                  
                  [self.navigationController popToRootViewControllerAnimated:TRUE];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [progressHUD hideUsingAnimation:YES];
              NSString* errorMessage = NSLocalizedString(@"RETAILER_DATA_UPDATE_ERROR", @"Password change error");
              [MLUtil showAlertMessage:errorMessage];
          }];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // Handle interaction
    switch (buttonIndex) {
        case 0: //Cancel
            break;
        case 1: //OK
            [self sendRequest];
            break;
    }
}

@end
