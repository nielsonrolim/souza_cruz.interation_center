//
//  MLCPFCNPJCodeField.m
//  RetailController
//
//  Created by Nielson Rolim on 9/1/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLCPFCNPJCodeField.h"
#import "MLUtil.h"

@implementation MLCPFCNPJCodeField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.delegate = self;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.delegate = self;
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.delegate = self;
    }
    return self;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *changedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    NSString *changedStringCleaned = [MLUtil cleanCNPJorCPF:changedString];
    
    NSString *filter = @"##############################";
    
    if (changedStringCleaned.length == 11) {
        filter = @"###.###.###-##";
    } else if (changedStringCleaned.length == 14) {
        filter = @"##.###.###/####-##";
    }
    
    if(!filter) return YES; // No filter provided, allow anything
    
    
    if(range.length == 1 && // Only do for single deletes
       string.length < range.length &&
       [[textField.text substringWithRange:range] rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]].location == NSNotFound)
    {
        // Something was deleted.  Delete past the previous number
        NSInteger location = changedString.length-1;
        if(location > 0)
        {
            for(; location > 0; location--)
            {
                if(isdigit([changedString characterAtIndex:location]))
                {
                    break;
                }
            }
            changedString = [changedString substringToIndex:location];
        }
    }
    
    textField.text = [MLUtil formatString:changedString WithMask:filter];
    
    return NO;
}

@end
