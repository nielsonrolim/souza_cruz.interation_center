//
//  MLOutletSegue.h
//  RetailController
//
//  Created by Delano Oliveira on 14/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 A Custom segue to simulate a IBOutlet to ViewControllers
*/

@interface MLOutletSegue : UIStoryboardSegue

@end
