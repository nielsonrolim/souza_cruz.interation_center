//
//  MLTextField.m
//  RetailController
//
//  Created by Nielson Rolim on 8/26/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLTextField.h"

@implementation MLTextField

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commomInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commomInit];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self commomInit];
    }
    return self;
}

// Add a button done and force EmailAddress type
- (void)commomInit {
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                target:self
                                                                                action:@selector(donePressed:)];
    UIBarButtonItem *flexibleSpaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                       target:nil
                                                                                       action:nil];
    
    keyboardDoneButtonView.items = [NSArray arrayWithObjects:flexibleSpaceItem, doneButton, nil];
    self.inputAccessoryView = keyboardDoneButtonView;
}

// Dismiss itself when done button is pressed
- (void)donePressed:(id)sender
{
    [self resignFirstResponder];
}

@end
