//
//  MLAppDelegate.h
//  RetailController
//
//  Created by Delano Oliveira on 29/07/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLRetailer.h"

@interface MLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

//Retailer object shared to all application
@property (strong, nonatomic) MLRetailer* retailer;

@end
