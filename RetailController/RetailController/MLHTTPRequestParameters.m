//
//  MLHTTPRequestOperationManager.m
//  RetailController
//
//  Created by Delano Oliveira on 13/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLHTTPRequestParameters.h"

@interface MLHTTPRequestParameters()

@property (nonatomic, strong) NSMutableDictionary *parameters;

@end

@implementation MLHTTPRequestParameters


#pragma mark - Class Methods

static NSMutableDictionary *defaultParameters;

+ (void)initialize
{
    defaultParameters = [NSMutableDictionary dictionaryWithDictionary:@{kSERVER_PARAMETER_KEY: kSERVER_VALUE_KEY}];
}

+ (NSDictionary *)defaultParameters
{
    return [defaultParameters copy];
}

+ (void)addDefaultParameterKey:(NSString *)key andValue:(id)value
{
    [defaultParameters setObject:value forKey:key];
}

+ (void)removeDefaultParameterKey:(NSString *)key
{
    [defaultParameters removeObjectForKey:key];
}

#pragma mark - Instance Methods

- (id)init
{
    self = [super init];
    if (self){
        self.parameters = [NSMutableDictionary dictionaryWithDictionary:defaultParameters];
    }
    return self;
}

- (void)addParameterKey:(NSString *)key andValue:(id)value
{
    [self.parameters setValue:value forKey:key];
}

- (void)addParametersFromDictionary:(NSDictionary *)dictionary
{
    [self.parameters setValuesForKeysWithDictionary:dictionary];
}

- (void)removeParameterKey:(NSString *)key
{
    [self.parameters setValue:nil forKey:key];
}

- (NSDictionary *)dictionary
{
    return self.parameters.copy;
}

@end
