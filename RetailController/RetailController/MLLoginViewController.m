//
//  MLLoginViewController.m
//  RetailController
//
//  Created by Delano Oliveira on 29/07/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLLoginViewController.h"
#import "MLHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "MLRetailer.h"
#import "MLUtil.h"
#import "MLAppDelegate.h"

@interface MLLoginViewController ()

@property (nonatomic) BOOL userOlderThan18;

@end

@implementation MLLoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Set border color of request password button
    self.requestPasswordButton.layer.borderColor = [[UIColor whiteColor] CGColor];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.passwordTextField.text = @"";
    [self.olderThan18Button setImage:[UIImage imageNamed:@"btn_unchecked.png"]
                            forState:UIControlStateNormal];
    self.userOlderThan18 = NO;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"sendToHome"]){
        
    }
}


- (IBAction)loginButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if (self.userOlderThan18) {
        MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        MLHTTPRequestOperationManager *manager = [MLHTTPRequestOperationManager manager];
        MLHTTPRequestParameters *parameters = [MLHTTPRequestParameters new];
        
        NSNumber* username = [NSNumber numberWithLongLong:[[MLUtil cleanCNPJorCPF:self.usernameTextField.text] longLongValue]];
        
        [parameters addParameterKey:kSERVER_PARAMETER_USERNAME andValue:username];
        [parameters addParameterKey:kSERVER_PARAMETER_PASSWORD andValue:self.passwordTextField.text];
        
        [manager POST:kSERVER_LOGIN_SERVICE_PATH
           parameters:parameters.dictionary
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  [progressHUD hideUsingAnimation:YES];
                  NSDictionary *json = responseObject;
                  if(json[kSERVER_JSON_DATA_PATH]){
                      MLRetailer *retailer = [[MLRetailer alloc] initWithDictionary:json[kSERVER_JSON_DATA_PATH]];
                      
                      MLAppDelegate* appDelegate = (MLAppDelegate*)[[UIApplication sharedApplication] delegate];
                      appDelegate.retailer = retailer;
                      [self performSegueWithIdentifier:@"sendToHome" sender:sender];
                  }
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  [progressHUD hideUsingAnimation:YES];
                  NSString* errorMessage = NSLocalizedString(@"LOGIN_INCORRECT", @"Login incorrect");
                  [MLUtil showAlertMessage:errorMessage];
              }];
    } else {
        NSString* errorMessage = NSLocalizedString(@"USER_UNDERAGE", @"User underage");
        [MLUtil showAlertMessage:errorMessage];
    }
}

- (IBAction)olderThan18ButtonPressed:(UIButton *)sender {
    [self.view endEditing:YES];
    if (!self.userOlderThan18) {
        [self.olderThan18Button setImage:[UIImage imageNamed:@"btn_checked.png"]
                forState:UIControlStateNormal];
        self.userOlderThan18 = YES;
    } else {
        [self.olderThan18Button setImage:[UIImage imageNamed:@"btn_unchecked.png"]
                forState:UIControlStateNormal];
        self.userOlderThan18 = NO;
    }
}

@end
