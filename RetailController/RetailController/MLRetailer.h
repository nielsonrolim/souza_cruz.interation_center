//
//  MLRetailer.h
//  RetailController
//
//  Created by Delano Oliveira on 29/07/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLEntity.h"

/*
 Represent a Retailer (the app user)
*/

@interface MLRetailer : MLEntity

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *company_name;
@property (nonatomic, strong) NSString *fancy_name;
@property (nonatomic, strong) NSString *seller_name;
@property (nonatomic, strong) NSString *phone1;
@property (nonatomic, strong) NSString *phone2;
@property (nonatomic, strong) NSString *phone3;
@property (nonatomic, strong) NSString *visit_days;
@property (nonatomic, strong) NSString *delivery_days;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSNumber *terms_accepted;
@property (nonatomic, strong) NSNumber *change_password;
@property (nonatomic, strong) NSString *created_at;
@property (nonatomic, strong) NSString *updated_at;
@property (nonatomic, strong) NSString *deleted_at;

@end
