//
//  MLRequestXMLViewController.h
//  RetailController
//
//  Created by Nielson Rolim on 8/26/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLTextField.h"
#import "MLNumberPadTextField.h"

/*
 Controller to Request XML (Invoice) View
 */

@interface MLRequestXMLViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *fieldsScrollView;

//View that envolve all fields
@property (weak, nonatomic) IBOutlet UIView *fieldsView;
//E-mail Text Field
@property (weak, nonatomic) IBOutlet MLTextField *emailTextField;
//Initial Invoice or Bill Text Field
@property (weak, nonatomic) IBOutlet MLNumberPadTextField *invoiceTextField;
//Initial Plus Button
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
//Show Billet View Button
@property (weak, nonatomic) IBOutlet UIButton *billetViewButton;
//Last Gray Line of fieldsView
@property (weak, nonatomic) IBOutlet UIView *lastGrayLine;

//Send Button (Request XML)
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

//Button to add a new invoice field
- (IBAction)addInvoiceTextField:(UIButton *)sender;
//Action executed when Send Button is pressed
- (IBAction)sendButtonPressed:(UIButton *)sender;
@end
