//
//  MLRequestChangeRetailDataViewController.h
//  RetailController
//
//  Created by Nielson Rolim on 8/25/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 Controller to Request Change Retailer Data (Profile) View
 */

@interface MLRequestChangeRetailerDataViewController : UIViewController <UITextViewDelegate>

//TextView with the data to be changed
@property (weak, nonatomic) IBOutlet UITextView *changeDataTextView;

- (IBAction)sendButtonPressed:(id)sender;

@end
