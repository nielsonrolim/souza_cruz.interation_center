//
//  MLHTTPRequestOperationManager.m
//  RetailController
//
//  Created by Delano Oliveira on 13/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLHTTPRequestOperationManager.h"

@implementation MLHTTPRequestOperationManager

+ (id)manager
{
    return [[MLHTTPRequestOperationManager alloc] init];
}

- (id)init
{
    self = [super initWithBaseURL:[NSURL URLWithString:kSERVER_URL]];
    return self;
}

@end
