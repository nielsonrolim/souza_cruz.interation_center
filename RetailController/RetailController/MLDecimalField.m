//
//  MLDecimalField.m
//  RetailController
//
//  Created by Nielson Rolim on 8/27/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLDecimalField.h"

@implementation MLDecimalField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.delegate = self;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.delegate = self;
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.delegate = self;
    }
    return self;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //Formatter for current currency
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.locale = [NSLocale currentLocale];
    formatter.generatesDecimalNumbers = YES;
    formatter.maximumFractionDigits = 2;
    formatter.minimumFractionDigits = 2;
    formatter.maximumIntegerDigits = 12;
    formatter.alwaysShowsDecimalSeparator = YES;
    
    //Appeding the value of what the user typed to the textField.text
    NSString *changedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    //Cleaning other characters than numbers
    NSString *stringToBeFormatted = changedString;
    stringToBeFormatted = [stringToBeFormatted stringByReplacingOccurrencesOfString:@"." withString:@""];
    stringToBeFormatted = [stringToBeFormatted stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    //Formatting the hole thing
    NSString* formattedString = [formatter stringFromNumber:[NSNumber numberWithDouble:([stringToBeFormatted doubleValue]/100)]];
    
    //Assigning the formatted string to the textField
    textField.text = formattedString;
    
    //If backspace pressed, it delete the last character
    if ([string isEqualToString:@""]) {
        return YES;
    }
    //Otherwise, just ignore what was pressed
    return NO;
}

@end
