//
//  MLEntitie.h
//  RetailController
//
//  Created by Delano Oliveira on 13/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 Represent a entity. This class contain the commom code to entities.
*/

@interface MLEntity : NSObject

// Represent Entity id
@property (nonatomic, strong) NSString *id_entity;

// Set values from dictionary (JSON)
- (id)initWithDictionary:(NSDictionary*)dictionary;

@end
