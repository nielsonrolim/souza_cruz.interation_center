//
//  MLAboutViewController.h
//  RetailController
//
//  Created by Delano Oliveira on 29/07/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 Controller to About View
 */

@interface MLAboutViewController : UIViewController

// Set YES to hide cancel button. When call this controller using modal segue don't set it with YES.
@property (nonatomic) BOOL hideCancelButton;
//WebView for About Content
@property (weak, nonatomic) IBOutlet UIWebView *aboutWebView;
//WebView for Terms of Use
@property (weak, nonatomic) IBOutlet UIWebView *termsOfUseWebView;

@end
