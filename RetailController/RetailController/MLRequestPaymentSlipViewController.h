//
//  MLRequestPaymentSlipViewController.h
//  RetailController
//
//  Created by Nielson Rolim on 8/27/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 Controller to Payment Slip View
 */

@interface MLRequestPaymentSlipViewController : UIViewController <UITextFieldDelegate>

//E-mail Text Field
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
//Invoice Text Field
@property (weak, nonatomic) IBOutlet UITextField *invoiceTextField;
@property (weak, nonatomic) IBOutlet UITextField *valueTextField;
@property (weak, nonatomic) IBOutlet UITextField *dueDateTextField;

//Request Payment
- (IBAction)sendButtonPressed:(UIButton *)sender;

@end
