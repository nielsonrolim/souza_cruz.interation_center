//
//  MLRetailerDataViewController.h
//  RetailController
//
//  Created by Nielson Rolim on 8/25/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MLRetailerDataViewController : UIViewController

//Retailer Code Label
@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
//Retailer Company Name Label
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
//Retailer Username Label
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
//Retailer Fancy Name Label
@property (weak, nonatomic) IBOutlet UILabel *fancyNameLabel;
//Retailer Phone 1 Label
@property (weak, nonatomic) IBOutlet UILabel *phone1Label;
//Retailer Phone 2 Label
@property (weak, nonatomic) IBOutlet UILabel *phone2Label;
//Retailer Phone 3 Label
@property (weak, nonatomic) IBOutlet UILabel *phone3Label;
//Retailer E-mail Label
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
//Retailer Visit Days Label
@property (weak, nonatomic) IBOutlet UILabel *visitDaysLabel;
//Retailer Delivery Days Label
@property (weak, nonatomic) IBOutlet UILabel *deliveryDaysLabel;
//Retailer Seller Name Label
@property (weak, nonatomic) IBOutlet UILabel *sellerNameLabel;

@end
