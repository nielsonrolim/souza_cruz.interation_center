//
//  MLUtil.h
//  RetailController
//
//  Created by Delano Oliveira on 14/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 This class contains commons methods to some classes.
*/

@interface MLUtil : NSObject

// Show a AlertView
+ (void)showAlertMessage:(NSString*)message withTitle:(NSString*)title;
+ (void)showAlertMessage:(NSString*)message;

//Confirm Dialog
+ (UIAlertView*) confirmDialog:(NSString*) message withTitle:(NSString*) title cancelText:(NSString*)cancelString andConfirmText:(NSString*)confirmString;
+ (UIAlertView*) confirmDialog:(NSString*) message;
+ (UIAlertView*) confirmDialog;

//Method for reposition an element in view
+ (void) reposition:(UIView*)element increasingXBy:(int)x_points andYBy:(int)y_points;

//Method for resize a element in view
+ (void) resize:(UIView*)element incresingWidthBy:(int)width_points andHeightBy:(int)height_points;

//Method for format a string according a mask
+ (NSMutableString*) formatString:(NSString*)string WithMask:(NSString*)filter;

//Method for stripping ".", "-" and "/" from a CNPJ or CPF
+ (NSString*) cleanCNPJorCPF:(NSString*)maskedNumber;

//Method to format CNPJ or CPF depeding the number of chars
+ (NSString*) formatCNPJorCPF:(NSString*)number;

//Method to validate an e-mail address using regular expressions
+ (BOOL) validateEmail:(NSString*) emailAddress;

//Method to create an UIImage from a given URL
+ (UIImage*) imageFromURL:(NSString*)url;

//Scroll a scrollView to bottom
+ (void) scrollToBottom:(UIScrollView*)scrollView;
@end
