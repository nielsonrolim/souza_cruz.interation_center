//
//  MLMenuViewController.m
//  RetailController
//
//  Created by Delano Oliveira on 14/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLMenuViewController.h"

@interface MLMenuViewController ()

@end

@implementation MLMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IBActions

- (void)logoutButtonPressed:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(menuViewControllerDidLogoutButtonPressed:)]){
        [self.delegate menuViewControllerDidLogoutButtonPressed:self];
    }
}

- (void)homeButtonPressed:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(menuViewControllerDidHomeButtonPressed:)]){
        [self.delegate menuViewControllerDidHomeButtonPressed:self];
    }
}

- (void)accountButtonPressed:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(menuViewControllerDidAccountButtonPressed:)]){
        [self.delegate menuViewControllerDidAccountButtonPressed:self];
    }
}

- (void)appRateButtonPressed:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(menuViewControllerDidAppRateButtonPressed:)]){
        [self.delegate menuViewControllerDidAppRateButtonPressed:self];
    }
}

- (void)aboutButtonPressed:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(menuViewControllerDidAboutButtonPressed:)]){
        [self.delegate menuViewControllerDidAboutButtonPressed:self];
    }
}

@end
