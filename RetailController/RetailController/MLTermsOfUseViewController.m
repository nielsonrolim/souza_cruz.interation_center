//
//  MLTermsOfdUseViewController.m
//  RetailController
//
//  Created by Nielson Rolim on 8/20/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLTermsOfUseViewController.h"
#import "MLAppDelegate.h"
#import "MLRetailer.h"
#import "MBProgressHUD.h"
#import "MLHTTPRequestOperationManager.h"
#import "MLUtil.h"

@interface MLTermsOfUseViewController ()

//Shared Retailer Object
@property (weak, nonatomic) MLRetailer* retailer;

@end

@implementation MLTermsOfUseViewController

- (MLRetailer*) retailer {
    if (!_retailer) {
        MLAppDelegate* appDelegate = (MLAppDelegate*)[[UIApplication sharedApplication] delegate];
        _retailer = appDelegate.retailer;
    }
    return _retailer;
}

- (void) getTermsOfUse {
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    MLHTTPRequestOperationManager *manager = [MLHTTPRequestOperationManager manager];
    
    MLHTTPRequestParameters *parameters = [MLHTTPRequestParameters new];
    
    [manager GET:kSERVER_GET_TERMS_OF_USE_PATH
      parameters:parameters.dictionary
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSDictionary *json = responseObject;
             if(json[kSERVER_JSON_DATA_PATH]){
                 NSString* termsOfUse = json[kSERVER_JSON_DATA_PATH][@"text"];
                 if (termsOfUse != nil && ![termsOfUse isKindOfClass:[NSNull class]]) {
                     [self.termsOfUseWebView loadHTMLString:termsOfUse baseURL:nil];
//                     self.termsOfUseTextView.text = termsOfUse;
                 }
                 [progressHUD hideUsingAnimation:YES];
             }
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [progressHUD hideUsingAnimation:YES];
         }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.termsOfUseWebView.backgroundColor = [UIColor clearColor];
    [self getTermsOfUse];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)sendButtonPressed:(UIButton *)sender {
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    MLHTTPRequestOperationManager *manager = [MLHTTPRequestOperationManager manager];
    
    MLHTTPRequestParameters *parameters = [MLHTTPRequestParameters new];
    [parameters addParameterKey:kSERVER_PARAMETER_TOKEN
                       andValue:self.retailer.token];
    
    [manager POST:kSERVER_TERMS_OF_USE_ACCEPT_PATH
       parameters:parameters.dictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              [progressHUD hideUsingAnimation:YES];
              NSDictionary *json = responseObject;
              if(json[kSERVER_JSON_DATA_PATH]){
                  self.retailer.terms_accepted = [NSNumber numberWithInt:1];
                  [self dismissViewControllerAnimated:YES completion:nil];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"error: %@", error);
              [progressHUD hideUsingAnimation:YES];
              NSString* errorMessage = NSLocalizedString(@"TERMS_OF_USE_ACCEPT_ERROR", @"Error while trying to accept terms of use");
              [MLUtil showAlertMessage:errorMessage];
          }];
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
