//
//  MLTextView.h
//  RetailController
//
//  Created by Nielson Rolim on 8/26/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 A custom TextField to input done button
 */

@interface MLTextView : UITextView

@end
