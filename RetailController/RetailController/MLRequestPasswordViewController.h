//
//  MLRequestPasswordViewController.h
//  RetailController
//
//  Created by Delano Oliveira on 13/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 Controller to Request Password View
*/

@interface MLRequestPasswordViewController : UIViewController

// TextField to type Email
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
// TextField to type Username (CNPJ / CPF)
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;

// Action to request password button
- (IBAction)requestPasswordButtonPressed:(id)sender;
// Action to cancel button
- (IBAction)cancelButtonPressed:(id)sender;
@end
