//
//  MLTermsOfdUseViewController.h
//  RetailController
//
//  Created by Nielson Rolim on 8/20/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 Controller to Terms of Use View
 */

@interface MLTermsOfUseViewController : UIViewController

//WebView for Terms of Use
@property (weak, nonatomic) IBOutlet UIWebView *termsOfUseWebView;

// Action to cancel button
- (IBAction)cancelButtonPressed:(id)sender;

// Method triggered when send button is pressed
- (IBAction)sendButtonPressed:(UIButton *)sender;

@end
