//
//  MLRetailerDataViewController.m
//  RetailController
//
//  Created by Nielson Rolim on 8/25/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLRetailerDataViewController.h"
#import "MLAppDelegate.h"
#import "MLRetailer.h"
#import "MLUtil.h"

@interface MLRetailerDataViewController ()

//Retailer object from App Delegate
@property (strong, nonatomic) MLRetailer* retailer;

//Retailer Code (CPF, CNPJ or Code)
@property (weak, nonatomic) IBOutlet UILabel *retailerCodeLabel;

@end

@implementation MLRetailerDataViewController

- (MLRetailer*) retailer {
    if (!_retailer) {
        MLAppDelegate* appDelegate = (MLAppDelegate*)[[UIApplication sharedApplication] delegate];
        _retailer = appDelegate.retailer;
    }
    return _retailer;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    if (self.retailer.username.length == 11) {  // username is a CPF
        NSString* filter = @"###.###.###-##";
        self.retailerCodeLabel.text = @"CPF:";
        self.usernameLabel.text = [MLUtil formatString:self.retailer.username WithMask:filter];
    } else if (self.retailer.username.length == 14) { // username is a CNPJ
        NSString* filter = @"##.###.###/####-##";
        self.retailerCodeLabel.text = @"CNPJ:";
        self.usernameLabel.text = [MLUtil formatString:self.retailer.username WithMask:filter];
    } else { // username is a Retailer Code
        self.retailerCodeLabel.text = NSLocalizedString(@"RETAILER_CODE", @"Retailer Code");
        self.usernameLabel.text = self.retailer.username;
    }
    
    self.codeLabel.text = self.retailer.code;
    self.companyNameLabel.text = self.retailer.company_name;
    self.fancyNameLabel.text = self.retailer.fancy_name;
    self.phone1Label.text = self.retailer.phone1;
    self.phone2Label.text = self.retailer.phone2;
    self.phone3Label.text = self.retailer.phone3;
    self.emailLabel.text = self.retailer.email;
    self.visitDaysLabel.text = self.retailer.visit_days;
    self.deliveryDaysLabel.text = self.retailer.delivery_days;
    self.sellerNameLabel.text = self.retailer.seller_name;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
