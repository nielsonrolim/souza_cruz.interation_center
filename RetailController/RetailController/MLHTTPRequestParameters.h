//
//  MLHTTPRequestOperationManager.h
//  RetailController
//
//  Created by Delano Oliveira on 13/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 This class create a NSMutableDictionary and add default parameters
 */

@interface MLHTTPRequestParameters : NSObject

// Add other commom parameter to all new dictionary
+ (void)addDefaultParameterKey:(NSString*)key andValue:(id)value;
// Remove a commom parameter to all new dictionary
+ (void)removeDefaultParameterKey:(NSString*)key;
// Return a dictionary with default parameters
+ (NSDictionary*)defaultParameters;

// Add or replace a parameter
- (void)addParameterKey:(NSString*)key andValue:(id)value;
// Add or replace parameters from a dictionary
- (void)addParametersFromDictionary:(NSDictionary*)dictionary;
// Remove a parameter
- (void)removeParameterKey:(NSString*)key;
// Return a dictionary with all parameters
- (NSDictionary*)dictionary;


@end
