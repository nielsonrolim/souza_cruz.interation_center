//
//  MLNumberPadTextField.m
//  RetailController
//
//  Created by Delano Oliveira on 14/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLNumberPadTextField.h"

@implementation MLNumberPadTextField

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self forceKeyboard];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self forceKeyboard];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self forceKeyboard];
    }
    return self;
}

// Force keyboard type to be a Number Pad
- (void)forceKeyboard
{
    self.keyboardType = UIKeyboardTypeNumberPad;
}


@end
