//
//  MLRequestXMLViewController.m
//  RetailController
//
//  Created by Nielson Rolim on 8/26/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLRequestXMLViewController.h"
#import "MLAppDelegate.h"
#import "MLRetailer.h"
#import "MBProgressHUD.h"
#import "MLHTTPRequestOperationManager.h"
#import "MLUtil.h"

@interface MLRequestXMLViewController ()

//Shared Retailer Object
@property (weak, nonatomic) MLRetailer* retailer;

//Invoice fields created programatically
@property (strong, nonatomic) NSMutableArray* invoices;
//Plus buttons created programatically (each button in this array corresponds to a invoice field in invoices array)
@property (strong, nonatomic) NSMutableArray* plusButtons;
//Minus buttons created programatically (each button in this array corresponds to a invoice field in invoices array)
@property (strong, nonatomic) NSMutableArray* minusButtons;
//Gray lines created programatically (each button in this array corresponds to a invoice field in invoices array)
@property (strong, nonatomic) NSMutableArray* grayLines;
//Billet View
@property (weak, nonatomic) IBOutlet UIView *billetView;

//Method to show Billet View
- (IBAction)showBilletView:(UIButton *)sender;

//Method to hide Billet View
- (IBAction)hideBilletView:(UIButton *)sender;

- (void) removeInvoiceTextField:(UIButton *)sender;

@end

@implementation MLRequestXMLViewController

- (MLRetailer*) retailer {
    if (!_retailer) {
        MLAppDelegate* appDelegate = (MLAppDelegate*)[[UIApplication sharedApplication] delegate];
        _retailer = appDelegate.retailer;
    }
    return _retailer;
}

- (NSMutableArray*) invoices {
    if (!_invoices) {
        _invoices = [[NSMutableArray alloc] init];
    }
    return _invoices;
}

- (NSMutableArray*) plusButtons {
    if (!_plusButtons) {
        _plusButtons = [[NSMutableArray alloc] init];
    }
    return _plusButtons;
}

- (NSMutableArray*) minusButtons {
    if (!_minusButtons) {
        _minusButtons = [[NSMutableArray alloc] init];
    }
    return _minusButtons;
}

- (NSMutableArray*) grayLines {
    if (!_grayLines) {
        _grayLines = [[NSMutableArray alloc] init];
    }
    return _grayLines;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.invoiceTextField.delegate = self;
    [self.invoices addObject:self.invoiceTextField];
    self.plusButton.enabled = NO;
    [self.plusButtons addObject:self.plusButton];
    [self.minusButtons addObject:self.billetViewButton];
    [self.grayLines addObject:self.lastGrayLine];
}

- (IBAction)addInvoiceTextField:(UIButton *)sender {
    [self.view endEditing:YES];
    
    MLNumberPadTextField *lastInvoiceTextField = [self.invoices lastObject];
    if (![lastInvoiceTextField.text isEqualToString:@""]) {
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        int fieldsViewHeight = self.fieldsView.frame.size.height;
        int sizeOfField = 50;
        int paddingButtonSpace = 40;
        int paddingTop = 138;
        int maxSizeToExpand = (window.frame.size.height - paddingTop) - (self.sendButton.frame.size.height + paddingButtonSpace);
        
        [MLUtil resize:self.fieldsView incresingWidthBy:0 andHeightBy:sizeOfField];
        
        //Creating the Text Field for the new invoice number
        MLNumberPadTextField *textField = [[MLNumberPadTextField alloc] initWithFrame:CGRectMake(10, fieldsViewHeight+9, 214, 33)];
        textField.borderStyle = UITextBorderStyleNone;
        textField.placeholder = @"NÚMERO DA NOTA";
        textField.font = [UIFont systemFontOfSize:14];
        textField.delegate = self;
        [self.invoices addObject:textField];
        [self.fieldsView addSubview:textField];
        
        //UIView gray line
        UIView* grayLine = [[UIView alloc] initWithFrame:CGRectMake(0, fieldsViewHeight+sizeOfField-1, 320, 1)];
        grayLine.backgroundColor = [UIColor blackColor];
        grayLine.alpha = 0.1;
        [self.grayLines addObject:grayLine];
        [self.fieldsView addSubview:grayLine];
        
        //Creating the new button for adding a new invoice
        [[self.plusButtons lastObject] setEnabled:NO];
        UIButton* plusButton = [[UIButton alloc] initWithFrame:CGRectMake(232, fieldsViewHeight+9, 32, 32)];
        [plusButton setImage:[UIImage imageNamed:@"icon_plus.png"]
                    forState:UIControlStateNormal];
        [plusButton addTarget:self
                       action:@selector(addInvoiceTextField:)
             forControlEvents:UIControlEventTouchUpInside];
        plusButton.enabled = NO;
        [self.plusButtons addObject:plusButton];
        [self.fieldsView addSubview:plusButton];
        
        //Creating the new button for adding a new invoice
        UIButton* minusButton = [[UIButton alloc] initWithFrame:CGRectMake(275, fieldsViewHeight+9, 32, 32)];
        [minusButton setImage:[UIImage imageNamed:@"icon_minus.png"]
                    forState:UIControlStateNormal];
        [minusButton addTarget:self
                       action:@selector(removeInvoiceTextField:)
             forControlEvents:UIControlEventTouchUpInside];
        [self.minusButtons addObject:minusButton];
        [self.fieldsView addSubview:minusButton];

        if (fieldsViewHeight < maxSizeToExpand) {
            [MLUtil reposition:self.sendButton increasingXBy:0 andYBy:sizeOfField];
            [MLUtil resize:self.fieldsScrollView incresingWidthBy:0 andHeightBy:sizeOfField];
        } else {
            self.fieldsScrollView.contentSize = self.fieldsView.frame.size;
            [MLUtil scrollToBottom:self.fieldsScrollView];
        }
    } else {
        sender.enabled = NO;
    }
}

- (void) removeInvoiceTextField:(UIButton *)sender {
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    int fieldsViewHeight = self.fieldsView.frame.size.height;
    int sizeOfField = 50;
    int paddingTop = 138;
    int maxSizeToContract = (window.frame.size.height - paddingTop) - (self.sendButton.frame.size.height);
    
    int index = [self.minusButtons indexOfObject:sender];
    
    MLNumberPadTextField* invoiceTextField = [self.invoices objectAtIndex:index];
    UIButton* plusButton = [self.plusButtons objectAtIndex:index];
    UIButton* minusButton = sender;
    UIView* grayLine = [self.grayLines objectAtIndex:index];
    
    [self.invoices removeObject:invoiceTextField];
    [self.plusButtons removeObject:plusButton];
    [self.minusButtons removeObject:minusButton];
    [self.grayLines removeObject:grayLine];
    
    [invoiceTextField removeFromSuperview];
    [plusButton removeFromSuperview];
    [minusButton removeFromSuperview];
    [grayLine removeFromSuperview];
    
    for (int i = index; i < self.invoices.count; i++) {
        [MLUtil reposition:[self.invoices objectAtIndex:i] increasingXBy:0 andYBy:-sizeOfField];
        [MLUtil reposition:[self.plusButtons objectAtIndex:i] increasingXBy:0 andYBy:-sizeOfField];
        [MLUtil reposition:[self.minusButtons objectAtIndex:i] increasingXBy:0 andYBy:-sizeOfField];
        [MLUtil reposition:[self.grayLines objectAtIndex:i] increasingXBy:0 andYBy:-sizeOfField];
    }
    
    [MLUtil resize:self.fieldsView incresingWidthBy:0 andHeightBy:-sizeOfField];
    self.fieldsScrollView.contentSize = self.fieldsView.frame.size;
    
    if (fieldsViewHeight < maxSizeToContract) {
        [MLUtil reposition:self.sendButton increasingXBy:0 andYBy:-sizeOfField];
        [MLUtil resize:self.fieldsScrollView incresingWidthBy:0 andHeightBy:-sizeOfField];
    }

    [MLUtil scrollToBottom:self.fieldsScrollView];

    if (![[[self.invoices lastObject] text] isEqualToString:@""]) {
        [[self.plusButtons lastObject] setEnabled:YES];
    }

}

- (IBAction)sendButtonPressed:(UIButton *)sender {
    [self.view endEditing:YES];
    
    if (![self validateFields]) {
        return;
    }
    
    UIAlertView* confirm = [MLUtil confirmDialog];
    confirm.delegate = self;
    [confirm show];
}

- (BOOL) validateFields {
    NSString* errorTitle = NSLocalizedString(@"VALIDATION_ERROR_TITLE", @"Error Title");
    
    if ([self.emailTextField.text isEqualToString:@""]) {
        NSString* errorMessage = NSLocalizedString(@"EMAIL_BLANK", @"E-mail empty");
        [MLUtil showAlertMessage:errorMessage withTitle:errorTitle];
        return NO;
    }
    
    if (![MLUtil validateEmail:self.emailTextField.text]) {
        NSString* errorMessage = NSLocalizedString(@"EMAIL_INVALID", @"E-mail address is invalid");
        [MLUtil showAlertMessage:errorMessage withTitle:errorTitle];
        return NO;
    }
    
    for (int i = 0; i < self.invoices.count; i++) {
        MLNumberPadTextField* textField = [self.invoices objectAtIndex:i];
        if ([textField.text isEqualToString:@""]) {
            NSString* errorMessage = [NSString stringWithFormat:NSLocalizedString(@"INVOICE_BLANK_MULTIPLE_FIELDS", @"INVOICE_BLANK"), i+1];
            [MLUtil showAlertMessage:errorMessage withTitle:errorTitle];
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    NSUInteger index = [self.invoices indexOfObject:textField];
    [[self.plusButtons objectAtIndex:index] setEnabled:YES];
    
    return YES;
}

- (IBAction)showBilletView:(UIButton *)sender {
    self.billetView.hidden = NO;
}

- (IBAction)hideBilletView:(UIButton *)sender {
    self.billetView.hidden = YES;
}

- (void) sendRequest {
    NSMutableArray* invoiceNumbers = [[NSMutableArray alloc] init];
    
    for (MLNumberPadTextField* textField in self.invoices) {
        [invoiceNumbers addObject:textField.text];
    }
    
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    MLHTTPRequestOperationManager *manager = [MLHTTPRequestOperationManager manager];
    
    MLHTTPRequestParameters *parameters = [MLHTTPRequestParameters new];
    [parameters addParameterKey:kSERVER_PARAMETER_TOKEN
                       andValue:self.retailer.token];
    [parameters addParameterKey:kSERVER_REQUEST_XML_PARAMETER_EMAIL
                       andValue:self.emailTextField.text];
    [parameters addParameterKey:kSERVER_REQUEST_XML_PARAMETER_INVOICES
                       andValue:invoiceNumbers];
    
    [manager POST:kSERVER_REQUEST_XML_PATH
       parameters:parameters.dictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              //              NSLog(@"response: %@", responseObject);
              [progressHUD hideUsingAnimation:YES];
              NSDictionary *json = responseObject;
              if(json[kSERVER_JSON_DATA_PATH]){
                  NSString* successMessage = NSLocalizedString(@"REQUEST_XML_SUCCESS", @"XML requested successfully");
                  [MLUtil showAlertMessage:successMessage];
                  [self.navigationController popToRootViewControllerAnimated:TRUE];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //              NSLog(@"error: %@", error);
              [progressHUD hideUsingAnimation:YES];
              NSString* errorMessage = NSLocalizedString(@"REQUEST_XML_ERROR", @"XML request error");
              [MLUtil showAlertMessage:errorMessage];
          }];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // Handle interaction
    switch (buttonIndex) {
        case 0: //Cancel
            break;
        case 1: //OK
            [self sendRequest];
            break;
    }
}

@end
