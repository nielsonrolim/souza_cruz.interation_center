//
//  MLNonDeliveryViewController.h
//  RetailController
//
//  Created by Nielson Rolim on 8/25/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 Controller to Non Delivery View
 */

@interface MLNonDeliveryViewController : UIViewController

//Non Delivery Button Pressed
- (IBAction)sendButtonPressed:(UIButton *)sender;

@end
