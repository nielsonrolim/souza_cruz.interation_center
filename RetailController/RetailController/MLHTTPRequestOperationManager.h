//
//  MLHTTPRequestOperationManager.h
//  RetailController
//
//  Created by Delano Oliveira on 13/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"
#import "MLHTTPRequestParameters.h"

/*
 This class create a AFHTTPRequestOperationManager and set default path
 */

@interface MLHTTPRequestOperationManager : AFHTTPRequestOperationManager

@end
