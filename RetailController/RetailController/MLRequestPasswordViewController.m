//
//  MLRequestPasswordViewController.m
//  RetailController
//
//  Created by Delano Oliveira on 13/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLRequestPasswordViewController.h"
#import "MLHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "MLUtil.h"

@interface MLRequestPasswordViewController ()

@end

@implementation MLRequestPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (IBAction)requestPasswordButtonPressed:(id)sender {
    [self.view endEditing:YES];
    
    if (![self validateFields]) {
        return;
    }

    [self sendRequest];
}

- (IBAction)cancelButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) sendRequest {
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    MLHTTPRequestOperationManager *manager = [MLHTTPRequestOperationManager manager];
    
    NSNumber* username = [NSNumber numberWithLongLong:[[MLUtil cleanCNPJorCPF:self.usernameTextField.text] longLongValue]];
    
    NSLog(@"username: %@", username);
    
    MLHTTPRequestParameters *parameters = [MLHTTPRequestParameters new];
    [parameters addParameterKey:kSERVER_PARAMETER_EMAIL
                       andValue:self.emailTextField.text];
    [parameters addParameterKey:kSERVER_PARAMETER_USERNAME
                       andValue:username];
    
    [manager POST:kSERVER_CALL_PASSWORD_SERVICE_PATH
       parameters:parameters.dictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              [progressHUD hideUsingAnimation:YES];
              NSDictionary *json = responseObject;
              if(json[kSERVER_JSON_DATA_PATH]){
                  
                  NSString* successMessage = NSLocalizedString(@"PASSWORD_REQUEST_SUCCESS", @"Password request successfully");
                  [MLUtil showAlertMessage:successMessage];
                  [self dismissViewControllerAnimated:YES completion:nil];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"error: %@", error);
              [progressHUD hideUsingAnimation:YES];
              NSString* errorMessage = NSLocalizedString(@"PASSWORD_REQUEST_ERROR", @"Password request error");
              [MLUtil showAlertMessage:errorMessage];
          }];
}

- (BOOL) validateFields {
    NSString* errorTitle = NSLocalizedString(@"VALIDATION_ERROR_TITLE", @"Error Title");
    
    if ([self.usernameTextField.text isEqualToString:@""]) {
        NSString* errorMessage = NSLocalizedString(@"RETAILER_CODE_BLANK", @"Retailer code is invalid");
        [MLUtil showAlertMessage:errorMessage withTitle:errorTitle];
        return NO;
    }
    
    if ([self.emailTextField.text isEqualToString:@""]) {
        return YES;
    }
    
    if (![MLUtil validateEmail:self.emailTextField.text]) {
        NSString* errorMessage = NSLocalizedString(@"EMAIL_INVALID", @"E-mail address is invalid");
        [MLUtil showAlertMessage:errorMessage withTitle:errorTitle];
        return NO;
    }
    
    return YES;
}

@end
