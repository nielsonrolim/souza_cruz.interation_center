//
//  MLDrawerController.m
//  RetailController
//
//  Created by Delano Oliveira on 14/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLDrawerController.h"

@implementation MLDrawerController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.hasLeft)
        [self performSegueWithIdentifier:@"left" sender:nil];
    if(self.hasCenter)
        [self performSegueWithIdentifier:@"center" sender:nil];
    if(self.hasRight)
        [self performSegueWithIdentifier:@"right" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"left"]){
        [self setLeftDrawerViewController:segue.destinationViewController];
    }
    else if([segue.identifier isEqualToString:@"center"]){
        [self setCenterViewController:segue.destinationViewController];
    }
    else if([segue.identifier isEqualToString:@"right"]){
        [self setRightDrawerViewController:segue.destinationViewController];
    }
}

@end
