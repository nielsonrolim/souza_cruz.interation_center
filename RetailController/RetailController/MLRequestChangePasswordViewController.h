//
//  MLRequestChangePasswordViewController.h
//  RetailController
//
//  Created by Nielson Rolim on 8/20/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 Controller to Request Change Password View
 */

@interface MLRequestChangePasswordViewController : UIViewController

//Current Password TextField
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
//New Password TextField
@property (weak, nonatomic) IBOutlet UITextField *passwordNewTextField;
//New Password Confirmation TextField
@property (weak, nonatomic) IBOutlet UITextField *passwordNewConfirmationTextField;

- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)sendButtonPressed:(id)sender;

@end
