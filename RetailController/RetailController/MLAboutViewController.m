//
//  MLAboutViewController.m
//  RetailController
//
//  Created by Delano Oliveira on 29/07/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLAboutViewController.h"
#import "MBProgressHUD.h"
#import "MLHTTPRequestOperationManager.h"

@interface MLAboutViewController ()

@end

@implementation MLAboutViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(self.hideCancelButton){
        [self.navigationItem setRightBarButtonItem:nil];
    }
    self.aboutWebView.backgroundColor = [UIColor clearColor];
    [self getAbout];
    self.termsOfUseWebView.backgroundColor = [UIColor clearColor];
    [self getTermsOfUse];
    self.aboutWebView.backgroundColor = [UIColor clearColor];
    [self getAbout];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) getAbout {
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    MLHTTPRequestOperationManager *manager = [MLHTTPRequestOperationManager manager];
    
    MLHTTPRequestParameters *parameters = [MLHTTPRequestParameters new];
    
    [manager GET:kSERVER_GET_ABOUT
      parameters:parameters.dictionary
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSDictionary *json = responseObject;
             if(json[kSERVER_JSON_DATA_PATH]){
                 NSString* aboutContent = json[kSERVER_JSON_DATA_PATH][@"text"];
                 if (aboutContent != nil && ![aboutContent isKindOfClass:[NSNull class]]) {
                     [self.aboutWebView loadHTMLString:aboutContent baseURL:nil];
                     //                     self.termsOfUseTextView.text = termsOfUse;
                 }
                 [progressHUD hideUsingAnimation:YES];
             }
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [progressHUD hideUsingAnimation:YES];
         }];
}

- (void) getTermsOfUse {
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    MLHTTPRequestOperationManager *manager = [MLHTTPRequestOperationManager manager];
    
    MLHTTPRequestParameters *parameters = [MLHTTPRequestParameters new];
    
    [manager GET:kSERVER_GET_TERMS_OF_USE_PATH
      parameters:parameters.dictionary
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSDictionary *json = responseObject;
             if(json[kSERVER_JSON_DATA_PATH]){
                 NSString* termsOfUse = json[kSERVER_JSON_DATA_PATH][@"text"];
                 if (termsOfUse != nil && ![termsOfUse isKindOfClass:[NSNull class]]) {
                     [self.termsOfUseWebView loadHTMLString:termsOfUse baseURL:nil];
                     //                     self.termsOfUseTextView.text = termsOfUse;
                 }
                 [progressHUD hideUsingAnimation:YES];
             }
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             [progressHUD hideUsingAnimation:YES];
         }];
}

@end
