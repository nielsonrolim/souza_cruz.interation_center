//
//  MLOtherMattersViewController.h
//  RetailController
//
//  Created by Nielson Rolim on 8/25/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 Controller to Other Matters View
 */

@interface MLOtherMattersViewController : UIViewController

//Description Text View
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

//Other matters button pressed
- (IBAction)sendButtonPressed:(UIButton *)sender;

@end
