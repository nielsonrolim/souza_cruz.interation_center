//
//  MLRequestChangePasswordViewController.m
//  RetailController
//
//  Created by Nielson Rolim on 8/20/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLRequestChangePasswordViewController.h"
#import "MLAppDelegate.h"
#import "MLRetailer.h"
#import "MBProgressHUD.h"
#import "MLHTTPRequestOperationManager.h"
#import "MLUtil.h"

@interface MLRequestChangePasswordViewController ()

//Shared Retailer Object
@property (weak, nonatomic) MLRetailer* retailer;

@end

@implementation MLRequestChangePasswordViewController

- (MLRetailer*) retailer {
    if (!_retailer) {
        MLAppDelegate* appDelegate = (MLAppDelegate*)[[UIApplication sharedApplication] delegate];
        _retailer = appDelegate.retailer;
    }
    return _retailer;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)sendButtonPressed:(id)sender {
    [self.view endEditing:YES];
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    MLHTTPRequestOperationManager *manager = [MLHTTPRequestOperationManager manager];
    
    MLHTTPRequestParameters *parameters = [MLHTTPRequestParameters new];
    [parameters addParameterKey:kSERVER_PARAMETER_TOKEN
                       andValue:self.retailer.token];
    [parameters addParameterKey:kSERVER_PARAMETER_PASSWORD
                       andValue:self.passwordTextField.text];
    [parameters addParameterKey:kSERVER_PARAMETER_NEW_PASSWORD
                       andValue:self.passwordNewTextField.text];
    [parameters addParameterKey:kSERVER_PARAMETER_CONFIRM_NEW_PASSWORD
                       andValue:self.passwordNewConfirmationTextField.text];
    
    [manager POST:kSERVER_CALL_PASSWORD_CHANGE_PATH
       parameters:parameters.dictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              [progressHUD hideUsingAnimation:YES];
              NSDictionary *json = responseObject;
              if(json[kSERVER_JSON_DATA_PATH]){
                  NSString* successMessage = NSLocalizedString(@"PASSWORD_CHANGE_SUCCESS", @"Password changed successfully");
                  [MLUtil showAlertMessage:successMessage];
                  self.retailer.change_password = [NSNumber numberWithInt:0];
                  [self dismissViewControllerAnimated:YES completion:nil];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [progressHUD hideUsingAnimation:YES];
              NSString* errorMessage = NSLocalizedString(@"PASSWORD_CHANGE_ERROR", @"Password change error");
              [MLUtil showAlertMessage:errorMessage];
          }];
}
@end
