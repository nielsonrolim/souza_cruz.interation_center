//
//  MLRateAppViewController.h
//  RetailController
//
//  Created by Nielson Rolim on 8/19/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLPickerField.h"

 /*
 Controller to RateApp View
 */

@interface MLRateAppViewController : UIViewController <UITextViewDelegate>

//TextField for categories Picker View
@property (weak, nonatomic) IBOutlet MLPickerField *categoryTextField;
// Commnets Text View
@property (weak, nonatomic) IBOutlet UITextView *commentsTextView;
//E-mail TextField
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

//Send button to avaliate the app
- (IBAction)sendButtonPressed:(UIButton *)sender;

@end
