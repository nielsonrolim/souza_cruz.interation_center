//
//  MLRequestProductViewController.h
//  RetailController
//
//  Created by Nielson Rolim on 8/14/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>


/*
 Controller to Request Product Call View
*/

@interface MLRequestProductCallViewController : UIViewController

//Request Product Pressed
- (IBAction)sendButtonPressed:(UIButton *)sender;

@end
