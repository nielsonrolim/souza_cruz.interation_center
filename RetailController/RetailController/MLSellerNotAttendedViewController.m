//
//  MLSellerNotAttendedViewController.m
//  RetailController
//
//  Created by Nielson Rolim on 8/25/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLSellerNotAttendedViewController.h"
#import "MLAppDelegate.h"
#import "MLRetailer.h"
#import "MBProgressHUD.h"
#import "MLHTTPRequestOperationManager.h"
#import "MLUtil.h"

@interface MLSellerNotAttendedViewController ()

//Shared Retailer Object
@property (weak, nonatomic) MLRetailer* retailer;

@end

@implementation MLSellerNotAttendedViewController

- (MLRetailer*) retailer {
    if (!_retailer) {
        MLAppDelegate* appDelegate = (MLAppDelegate*)[[UIApplication sharedApplication] delegate];
        _retailer = appDelegate.retailer;
    }
    return _retailer;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)sendButtonPressed:(UIButton *)sender {
    UIAlertView* confirm = [MLUtil confirmDialog];
    confirm.delegate = self;
    [confirm show];
}

- (void) sendRequest {
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    MLHTTPRequestOperationManager *manager = [MLHTTPRequestOperationManager manager];
    
    MLHTTPRequestParameters *parameters = [MLHTTPRequestParameters new];
    [parameters addParameterKey:kSERVER_PARAMETER_TOKEN
                       andValue:self.retailer.token];
    
    [manager POST:kSERVER_SELLER_NOT_ATTENDED_PATH
       parameters:parameters.dictionary
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              [progressHUD hideUsingAnimation:YES];
              NSDictionary *json = responseObject;
              if(json[kSERVER_JSON_DATA_PATH]){
                  NSString* successMessage = NSLocalizedString(@"SELLER_NOT_ATTENDED_REQUEST_SUCCESS", @"Product requested successfully");
                  [MLUtil showAlertMessage:successMessage];
                  [self.navigationController popToRootViewControllerAnimated:TRUE];
              }
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [progressHUD hideUsingAnimation:YES];
              NSString* errorMessage = NSLocalizedString(@"SELLER_NOT_ATTENDED_REQUEST_ERROR", @"Product request error");
              [MLUtil showAlertMessage:errorMessage];
          }];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // Handle interaction
    switch (buttonIndex) {
        case 0: //Cancel
            break;
        case 1: //OK
            [self sendRequest];
            break;
    }
}


@end
