//
//  MLLoginViewController.h
//  RetailController
//
//  Created by Delano Oliveira on 29/07/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 Controller to Login View
 */

@interface MLLoginViewController : UIViewController

// Button to request password
@property (weak, nonatomic) IBOutlet UIButton *requestPasswordButton;
// TextField to type Username (CNPJ / Retail Code)
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
// TextField to type password
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
// Button olderThan18 - checkbox
@property (weak, nonatomic) IBOutlet UIButton *olderThan18Button;

// Action to do login
- (IBAction)loginButtonPressed:(id)sender;
- (IBAction)olderThan18ButtonPressed:(id)sender;

@end
