//
//  MLDrawerController.h
//  RetailController
//
//  Created by Delano Oliveira on 14/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MMDrawerController.h"

/*
 Improve the MMDrawerController (a side menu framework) to allow linking left, center and right from Storyboard using a custom segue
 You must naming the segue with left, center or right.
*/

@interface MLDrawerController : MMDrawerController

// Required set YES when linking a view controller (left, center or right)
@property (nonatomic) BOOL hasLeft;
@property (nonatomic) BOOL hasCenter;
@property (nonatomic) BOOL hasRight;

@end
